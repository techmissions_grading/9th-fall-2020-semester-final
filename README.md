# FINAL
## Instructions: 
You will be using the templates here as starter code. The index.html
file needs to represent a sign up page so there will need to be some changes. There is a response page that is completely done, you may look over this code, but it needs no action. Be as creative as you can be, remember you can use your dev tools in your web browser to get ideas from your favorite site. 

### Sign Up Page
#### Sign up page must include: 
- Image of company Logo (don't spend too much time on this) 
- Form to sign up
##### This needs to have labels and inputs.
- First Name
- Second Name
- Password
- Email
- Phone Number
- Birthday
- Age
##### List of types:
- button
- checkbox
- file
- hidden
- image
- password
- radio
- reset
- submit
- text
- number
- date
- email
- tel


### Home page must include: 
- Image of company Logo (don't spend too much time on this) 
- Nav bar at the top of the page with buttons that link to other pages. (This will be in the starter code for you.)
- Body of page should have: some relevant images and information about the business that you feel will help keep customers engaged or that you feel will recruit new customers.
#### List of Possible Elements 
- (h1 - h6) 
- title
- table
- form
- section
- article
- p
- button
- a
- input type="checkbox"
- div
- iframe
- ul
- ol

You will have until Sunday 12/15 to complete this portion of the assignment.  Create a log-in page and the first “home” page for your new website
